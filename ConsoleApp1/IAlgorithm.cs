﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    interface IAlgorithm
    {
        void Sort();
        int BinarySearch(int pint);
    }
}
