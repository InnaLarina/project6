﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int cntNode = 10;
            CycleList list = new CycleList(cntNode);
            int count = 0;
            foreach (Node i in list)
            {
                count++;
                System.Console.Write(i.Data);
                //if (count > 15) break;
            }
            Console.WriteLine();
            if (list.IsSorted()) 
                Console.WriteLine("sorted"); 
            else 
                Console.WriteLine("not sorted");

            Console.WriteLine("Sorted List:");
            list.Sort();
            
            count = 0;
            foreach (Node i in list)
            {
                count++;
                System.Console.Write(i.Data);
                //if (count > 15) break;
            }
            Console.WriteLine();
            if (list.IsSorted()) 
                Console.WriteLine("sorted"); 
            else 
                Console.WriteLine("not sorted");

            Console.WriteLine($"2 is on the place {list.BinarySearch(2)}");
            Console.WriteLine($"5 is on the place {list.BinarySearch(5)}");
            Console.WriteLine($"8 is on the place {list.BinarySearch(8)}");

            System.Console.WriteLine("\nDone");
        }
    }
}
