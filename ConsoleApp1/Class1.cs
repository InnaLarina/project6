﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public struct TSortStackItem
    {
        public int Level;
        public Node Item;
    }
    public class Node : IComparable
    {
        public Node(int t)
        {
            next = null;
            data = t;
        }
        private Node next;
        public Node Next
        {
            get { return next; }
            set { next = value; }
        }
        private int data;
        public int Data
        {
            get { return data; }
            set { data = value; }
        }
        public int CompareTo(object objNode)
        {
            if (objNode is Node)
            {
                Node node = objNode as Node;
                if (this.data > node.data)
                    return 1;
                if (this.data < node.data)
                    return -1;
                else
                    return 0;
            }
            else return 0;
        }
    }
}
