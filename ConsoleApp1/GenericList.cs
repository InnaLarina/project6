﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class CycleList: IEnumerable<Node>, IAlgorithm
    {
        public int cntNodes { get; set; }
        private Node head;
        private Node tail;
        public void AddHead(int t) 
        {
            Node n = new Node(t);
            n.Next = head;
            head = n;
            if (tail == null) 
                tail = n;
                
        }
        public CycleList(int cntNode) 
        {
            cntNodes = cntNode;
            Random random = new Random();
            for (int x = 0; x < cntNode; x++)
            {
                this.AddHead(random.Next(0,9));
            }
            tail.Next = head;
        }
        public void DoCycleList() //connect ring
        {
            tail = null;
            Node p = head;
            while (p != null)
            {
                tail = p;
                p = p.Next;
            }
            tail.Next = head;
        }
        public void BreakCycleList() //disconnect ring
        {
            tail.Next = null;
            tail = null;
        }
       
        public IEnumerator<Node> GetEnumerator()
        {
            Node curr = head;
            yield return curr;
            do
            {
                curr = curr.Next;
                yield return curr;
            }
            while (curr.Next != head);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool IsSorted() 
        {
            Node cur;
            Node p = head;
            while (p != null)
            {
                cur = p;
                p = p.Next;
                if (cur == tail) break;
                if (cur.CompareTo(p) == 1) return false;
            }
            return true;
            
        }
        public int BinarySearch(int value) 
        {
            Random random = new Random();
            if (!this.IsSorted()) 
                return random.Next(0, cntNodes) * (-1) - 1;
            if (head.Data > value) 
                return 0;
            if (tail.Data < value) 
                return (~cntNodes);
            Node p = head;
            int cnt = 0;
            while (p != null)
            {
                if (p.Data == value) return cnt;
                if (p.Next.Data > value) return ~(cnt+1);
                cnt++;
                if (p == tail) break;
                p = p.Next;
            }
            return (~cntNodes);
        }
        public void Sort()
        {
            Node IntersectSorted(Node pList1, Node pList2)
            {
                Node pCurItem, listResult;
                Node p1, p2;
                p1 = pList1;
                p2 = pList2;
                if (p1.Data <= p2.Data)
                {
                    pCurItem = p1;
                    p1 = p1.Next;
                }
                else
                {
                    pCurItem = p2;
                    p2 = p2.Next;
                }
                listResult = pCurItem;
                while ((p1 != null) && (p2 != null)) 
                    {
                        if (p1.Data <= p2.Data)
                        {
                            pCurItem.Next = p1;
                            pCurItem = p1;
                            p1 = p1.Next;
                        }
                        else
                        {
                            pCurItem.Next = p2;
                            pCurItem = p2;
                            p2 = p2.Next;
                        }
                    }
                 if (p1 != null)
                    pCurItem.Next = p1;
                 else
                    pCurItem.Next = p2;
                return listResult;
             };

            BreakCycleList();
            TSortStackItem[] Stack = new TSortStackItem[31];
            int StackPos;
            Node p;
            StackPos = 0;
            p = this.head;
            while (p != null)
            {
                Stack[StackPos].Level = 1;
                Stack[StackPos].Item = p;
                p = p.Next;
                Stack[StackPos].Item.Next = null;
                ++StackPos;
                while ((StackPos > 1) && (Stack[StackPos - 1].Level == Stack[StackPos - 2].Level))
                {
                    Stack[StackPos - 2].Item = IntersectSorted(Stack[StackPos - 2].Item, Stack[StackPos - 1].Item);
                    ++(Stack[StackPos - 2].Level);
                    --StackPos;
                }
            }
            while (StackPos > 1) 
            {
                Stack[StackPos - 2].Item= IntersectSorted(Stack[StackPos - 2].Item, Stack[StackPos - 1].Item);
                ++Stack[StackPos - 2].Level;
                --StackPos;
            }
            if (StackPos > 0)
              head = Stack[0].Item;
            
            DoCycleList();
        }

       
    }
}
